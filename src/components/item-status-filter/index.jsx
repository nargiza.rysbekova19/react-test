import React, { Component } from 'react';
// import './header.css';


export default class ItemStatusFilter extends Component {
    buttons = [

        { name: 'all', label: 'All' },
        { name: 'active', label: 'Active' },
        { name: 'done', label: 'Done' }
    ];

    render() {
        const { filter, onSearchFilter} = this.props

        const buttons = this.buttons.map(({name, label}) => {
            const isActive = filter == name;
            const clazz = isActive ? 'btn-info': 'btn-outline-secondary'
            return  (
                <button 
                type='button' 
                className={`btn ${clazz}`} 
                key={name}
                onClick={() => onSearchFilter(name)}>{label}</button>
                // <button type='button' className="btn btn-outline-secondary">{label}</button>,
                // <button type='button' className="btn btn-outline-secondary">{label}</button>
            );
        });

        return (
            <div className='btn-group'>
               {buttons}
            </div>
        );
    }
}



// Функц компонент

// import React from 'react'
// // import './header.css';


// export const ItemStatusFilter = () => {
//     return (
//         <div className='btn-group'>
//             <button type='button' className="btn btn-info">All</button>
//             <button type='button' className="btn btn-outline-secondary">Active</button>
//             <button type='button' className="btn btn-outline-secondary">Done</button>
//         </div>
//     )
// }