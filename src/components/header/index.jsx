import React from 'react'
import './header.css';


export const HeaderTodo = ({ toDo, done}) => {
    return (
        <div className='header-wrapper'>
            <h1 className='header'>React To Do List</h1>
            <h2>{toDo} more todo, {done} done</h2>
        </div>
    )

}