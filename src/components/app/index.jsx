import React, { Component } from 'react'
import { HeaderTodo } from '../header';
import ItemAddForm from '../item-add-form';
import ItemStatusFilter from '../item-status-filter/index';
import SearchPanel from '../search-panel';
import TodoList from '../todo-list';



export default class AppOne extends Component {

  maxId = 100;
  state = {
    todoData: [
      this.createTodoItem('Learn React'),
      this.createTodoItem('Learn English'),
      this.createTodoItem('Learn JS')
    ],
    term: '',
    filter: 'done'
  };

  deleteItem = (id) => {
    this.setState(({ todoData }) => {
      const inx = todoData.findIndex((el) => el.id == id);
      const newArray = [...todoData.slice(0, inx),
      ...todoData.slice(inx + 1)];
      return {
        todoData: newArray
      };
    });
    // console.log(id)
  };

  createTodoItem(label) {
    return {
      label,
      important: false,
      done: false,
      id: this.maxId++
    }
  }

  addItem = (text) => {
    const newItem = this.createTodoItem(text);
    // addItem = (text) => {      old version
    //   const newItem = {
    //     label: text,
    //     important: false,
    //     id: this.maxId++
    //   }

    this.setState(({ todoData }) => {
      const newArr = [
        ...todoData,
        newItem
      ];
      return {
        todoData: newArr
      }
    })
  }

  toggleProperty(arr, id, propName) {
    const inx = arr.findIndex((el) => el.id == id);

    // 1update object
    const oldItem = arr[inx];
    const newItem = {
      ...oldItem,
      [propName]: !oldItem[propName]
    }

    // new arr
    return [...arr.slice(0, inx),
      newItem,
    ...arr.slice(inx + 1)];

  }



  onToggleImportant = (id) => {
    this.setState(({ todoData }) => {
      return {
        todoData: this.toggleProperty(todoData, id, 'important')
      };
    })
  }
  onToggleDone = (id) => {
    this.setState(({ todoData }) => {
      return {
        todoData: this.toggleProperty(todoData, id, 'done')
      };

    })
  }
  // onToggleDone = (id) => {      old version
  //   this.setState(({ todoData }) => {
  //     const inx = todoData.findIndex((el) => el.id == id);

  //     // 1update object
  //     const oldItem = todoData[inx];
  //     const newItem = { ...oldItem, done: !oldItem.done }

  //     // new arr
  //     const newArray = [...todoData.slice(0, inx),
  //       newItem,
  //     ...todoData.slice(inx + 1)];
  //     return {
  //       todoData: newArray
  //     };

  //   })
  // }
  search(items, term) {
    if(term.length == 0){
      return items
    }
     return items.filter((item) =>{
     return  item.label.toLowerCase().indexOf(term.toLowerCase()) > 1
   })
  }

  onSearchChange = (term) =>{
    this.setState({term})
  }
  onSearchFilter = (filter) =>{
    this.setState({filter})
  }

  filter(items, filter){
    switch(filter){
      case 'all': 
      return items;
      case 'active': 
      return items.filter((item) => !item.done)
      case 'done': 
      return items.filter((item) => item.done)
      default: return items;
    }
  }


  render() {
    const { todoData, term, filter } = this.state
    const doneCount = todoData.filter((el) => el.done).length;
    const todoCount = todoData.length - doneCount;
    // const todoCount = this.state.todoData.length - doneCount; пример использования thisстейта

    const visibleItems = this.filter(
      this.search(todoData, term), filter);
    return (
      <div className="App" >
        <HeaderTodo toDo={todoCount} done={doneCount} />
        <SearchPanel onSearchChange={this.onSearchChange}/>
        <ItemStatusFilter filter={filter} onSearchFilter={this.onSearchFilter}/>
        <TodoList
          todos={visibleItems}
          onDeleted={this.deleteItem}
          onToggleImportant={this.onToggleImportant}
          onToggleDone={this.onToggleDone} />
        <ItemAddForm onAddedItem={this.addItem} />
      </div>
    );
  }
}



// export const AppOne = () => {

//   const todoData = [
//     { id: 1, label: 'Learn React', important: true},
//     { id: 2, label: 'Learn English words', important: false},
//     {id: 3, label: 'Learn JS', important: false}
//   ];

//   return (
//     <div className="App">
//       <HeaderTodo/>
//       <SearchPanel/>
//      <ItemStatusFilter/>
//       <TodoList todos={todoData} onDeleted={(id) => console.log( 'del' , id)} />
//     </div>
//   );
// }

