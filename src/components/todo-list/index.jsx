import React from 'react'
import TodoListItem from '../todo-list-item'
import './todo-list.css';



const TodoList = ({ todos, onDeleted, onToggleImportant, onToggleDone }) => {

    const elements = todos.map((item) => {
        // itemProps не имеет id, id вытащили отдельно
        const { id, ...itemProps } = item
        return (
            // если оставить ...item то, будет означать что item не будет использовать id, поэтому id вытащили
            <li key={id}
                className='list-group-item'>
                <TodoListItem {...itemProps}
                    onDeleted={() => onDeleted(id)} 
                    onToggleImportant={() => onToggleImportant(id)}
                    onToggleDone={() => onToggleDone(id)}/>
            </li>
        )
    })
    return (
        <div>
            <ul className='list-group todo-list'>
                {elements}
            </ul>
        </div>
    )
}

export default TodoList

// onDeleted={() => console.log('Deleted')} />