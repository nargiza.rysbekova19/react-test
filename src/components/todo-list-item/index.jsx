// import { render } from '@testing-library/react';
import React, { Component } from 'react'
import './todo-list-item.css';

export default class TodoListItem extends Component {

    // state = {
    //     done: false,
    //     important: false
    // };  и стейт не нужен
     

    // Эти две функции больше не нужны
    // onLabelClick = () => {
    //     // здесь идет деструктуризация done, вытащила из state   111
    //     this.setState(({ done }) => {
    //         return {
    //             done: !done
    //         }
    //     })
    //     // console.log(`Done ${this.props.label}`)
    // }

    // onMarkImportant = () => {
    //     // 2
    //     this.setState((state) => {
    //         return {
    //             important: !state.important
    //         }
    //     })
    // }


    render() {
        // деструктуризация, вытащила done из state
        const { label, onDeleted, onToggleImportant, onToggleDone, done, important } = this.props;
        // const { done, important } = this.state;  удалили потому что они уже приходят из App
        let classNames = 'todo-list-item'
        if (done) {
            classNames += ' done'
        }

        const style = {
            color: important ? 'steelBlue' : 'black',
            fontWeight: important ? 'bold' : 'normal'
        }
        return (
            <span className={classNames}>
                <span
                    className='todo-list-item-label '
                    style={style}
                    onClick={onToggleDone}>
                    {/* onClick={this.onLabelClick}>  старая версия  this.onMarkImportant*/}
                    {label}</span>
                <div>
                    <button onClick={onToggleImportant} type='button' className='btn btn-outline-success'><i className='fa fa-exclamation' /></button>
                    <button onClick={onDeleted} type='button' className='btn btn-outline-success'><i className='fa fa-trash-o' /></button>
                </div>

                {/* onClick = this.props.onDeleted
                     */}
            </span>

        )
    }

}

// Функц компонент
// import React from 'react'
// import './todo-list-item.css';

// const TodoListItem = ({ label, important = false }) => {

//     const style = {
//         color: important ? 'steelBlue' : 'black',
//         fontWeight: important ?  'bold' : 'normal'
//     }
//     return (
//         <div className='list-block'>
//             <span className='todo-list-item' style={style}>{label}</span>
//             <div>
//                 <button type='button' className='btn btn-outline-success'><i className='fa fa-exclamation' /></button>
//                 <button type='button' className='btn btn-outline-success'><i className='fa fa-trash-o' /></button>
//             </div>
//         </div>

//     )
// }

// export default TodoListItem