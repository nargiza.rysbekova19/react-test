import React, { useState, useEffect } from 'react';
import axios from 'axios'
//useForm библиотека для проверки валидации полей(инпутов)
import { useForm } from 'react-hook-form';
export function RegistrationForm() {
    //register метод который применяется на определенный инпут который нужно проверить на символы или на цифры и выдать ошибку
    //handleSubmit метод который собирает все данные из инпутов он вешается на form тэг    
    //error просто объэект который содержит ошибки в процессе заполнения инпутов
    //getValues метод который получает значение из определенного инпута обращаясь по имени (getValues().username)
    const { register, handleSubmit, errors, getValues } = useForm({});
    const [role, setRole] = useState('user');
    console.log(role)

    
    {/* 
    const [authData , setAuthData] = useState({
        email : "",
        password : "",
        confirmPassword: ""
    })
    useEffect(() => {
        console.log('state', authData);
    },[authData]); 
    const handleChange = (e) => {
        //объявление переменной таким образом просто сокращает код 
        // const {id , value} = e.target 
        //перемнная id содержит в себе input с id='email'  
        const id = e.target.id;
        //перемнная value содержит в себе input значение   
        const value = e.target.value;
        // console.log("значение:" + value); 
        // console.log("айди: " + id);   
        //authData spread оператор который принимает в себе массив переменных 
        //которые через setAuthData обновляются смотря по заданной id = e.target.id
        setAuthData(authData => ({
            ...authData, [id] : value 
        }))
    }
    const handleSubmitClick = (e) => {
        e.preventDefault();
        if(authData.password === authData.confirmPassword) {
            sendDetailsToServer()    
        } else {
            props.showError('Passwords do not match');
        }
    }
*/}

    const onSubmit = (data) => {
        console.log(data)
        axios.post(`https://jsonplaceholder.typicode.com/users`, { data })
        .then(res => {
          console.log(res);
          console.log(res.data);
        })
    }


    const handleOnChnageRole = (e) => {
        setRole(e.target.value);
    };

    return (
        <div className="card col-12 col-lg-4 login-card mt-2 hv-center">
            <form onSubmit={handleSubmit(onSubmit)} >
                <div className="form-group text-left">
                    <label htmlFor="exampleInputUsername1">Username</label>
                    <input
                        className="form-control"
                        aria-describedby="emailHelp"
                        name="username"
                        ref={register({
                            //поле должно быть заполнено 
                            required: "You must specify a username",
                            pattern: { value: /^[A-Za-z]+$/i, message: "your username must to use letters" },
                            minLength: { value: 6, message: "your name too short" },
                            maxLength: { value: 20, message: "your name too long" },
                        })}
                        style={{ borderColor: errors.username && "red" }}
                        placeholder="Username"
                    />
                    {errors.username && <p style={{ color: " red " }} >{errors.username.message}</p>}
                </div>
                <div className="form-group text-left">
                    <label htmlFor="exampleInputEmail1">Email address</label>
                    <input
                        className="form-control"
                        id="email"
                        aria-describedby="emailHelp"
                        placeholder="email"
                        name="email"
                        ref={register({
                            required: "You must specify a email",
                            pattern: {
                                value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                                message: "invalid email address"
                            }
                        })}
                        style={{ borderColor: errors.email && "red" }}
                        placeholder="email"
                    //    value={authData.email}
                    //    onChange={handleChange}
                    />
                    {errors.email && <p style={{ color: " red " }} >{errors.email.message}</p>}
                    <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small>
                </div>
                <div className="form-group text-left">
                    <label htmlFor="exampleInputPassword1">Password</label>
                    <input type="password"
                        className="form-control"
                        id="password"
                        placeholder="Password"
                        name="password"
                        ref={register({
                            required: "You must specify a password",
                            minLength: {
                                value: 8,
                                message: "Password must have at least 8 characters"
                            }
                        })}
                        placeholder="Password"
                        // value={authData.password}
                        // onChange={handleChange} 
                        oncopy="return false"
                        oncut="return false"
                    />
                    {errors.password && <p style={{ color: " red " }} >{errors.password.message}</p>}
                </div>
                <div className="form-group text-left">
                    <label htmlFor="exampleInputPassword1">Confirm Password</label>
                    <input type="password"
                        className="form-control"
                        id="confirmPassword"
                        name="confirmPassword"
                        ref={register({
                            required: "You must specify a Confirm Password",
                            validate: {
                                passwordEqual: value => (value === getValues().password) || "The passwords do not match"
                            }
                        })}
                        placeholder="Confirm Password"
                        // value={authData.confirmPassword}
                        // onChange={handleChange}
                        oncopy="return false"
                        oncut="return false"
                    />
                    {errors.confirmPassword && <p style={{ color: " red " }} >{errors.confirmPassword.message}</p>}
                </div>

                <div className='radio'>
                    <label>
                        <input 
                        ref={register({ required: 'This is required' })} 
                        name="user"  type='radio' id="2" value='user' 
                        checked={role === 'user'} onChange={handleOnChnageRole} />
                        User
                    </label>
    
                    <label>
                        <input    
                        type='radio' 
                        name="admin" 
                        id="1" 
                        value='admin' 
                        checked={role === 'admin'} 
                        onChange={handleOnChnageRole} />
                         Admin
                    </label>
                </div>
                <button type="submit" className="btn btn-primary">
                    Register
                </button>
            </form>
        </div>
    )
}



























// import React, {useState,useEffect} from 'react';
// export function RegistrationForm(props) {

//     const {register} = useForm();

//     const [authData , setAuthData] = useState({
//         email : "",
//         password : "",
//         confirmPassword : ""
//     })
//     useEffect(() => {
//         console.log('state', authData);
//     },[authData]); 

//     const handleChange = (e) => {
//         //объявление переменной таким образом просто сокращает код 
//         // const {id , value} = e.target 
//         //перемнная id содержит в себе input с id='email'  
//         const id = e.target.id;
//         //перемнная value содержит в себе input значение   
//         const value = e.target.value;
//         console.log("значение:" + value); 
//         console.log("айди: " + id);   
//         //authData spread оператор который принимает в себе массив переменных 
//         //которые через setAuthData обновляются смотря по заданной id = e.target.id
//         setAuthData(authData => ({
//             ...authData, [id] : value 
//         }))
//     }



//   return(
//         <div className="card col-12 col-lg-4 login-card mt-2 hv-center">
//             <form>
//                 <div className="form-group text-left">
//                 <label htmlFor="exampleInputEmail1">Email address</label>
//                 <input type="email" 
//                        className="form-control" 
//                        id="email" 
//                        aria-describedby="emailHelp" 
//                        placeholder="Enter email"

//                        value={authData.email}
//                        onChange={handleChange}
//                 />
//                 <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small>
//                 </div>
//                 <div className="form-group text-left">
//                     <label htmlFor="exampleInputPassword1">Password</label>
//                     <input type="password" 
//                         className="form-control" 
//                         id="password" 
//                         placeholder="Password"
//                         value={authData.password}
//                         onChange={handleChange} 
//                     />
//                 </div>
//                 <div className="form-group text-left">
//                     <label htmlFor="exampleInputPassword1">Confirm Password</label>
//                     <input type="password" 
//                         className="form-control" 
//                         id="confirmPassword" 
//                         placeholder="Confirm Password"
//                         value={authData.confirmPassword}
//                         onChange={handleChange} 
//                     />
//                 </div>
//                 <button type="submit" className="btn btn-primary">
//                     Register
//                 </button>
//             </form>
//         </div>
//     )
// }