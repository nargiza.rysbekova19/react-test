import React from 'react';
import   './Btn.css'
function  Btn() {

    const res = [{ name: "op 1" }, { name: "op 2" }];

    const Button = ({ message }) => {
        const [condition, setCondition] = React.useState(null);     
      
        return res.map((data, k) => (
          <button
            key={k}
            className={`button ${condition === k ? "toggled" : ""}`}
            onClick={() => setCondition(k)}
          >
            {data.name}
          </button>
        ));
      };




    return(
        <div>

            <Button/>
      
        </div>
    )
}
export default Btn;